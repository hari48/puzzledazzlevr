﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

public class PieceController : MonoBehaviour
{
    private float fall = 0;
    public float fallSpeed = 0.8f;
    private GameObject pointer_Highlight;
    private GameObject anchor;
    public GameObject holder;
    public float moveOffset;
    private bool controlPiece = true;
    public bool makeMove = false;
    private bool pieceSettled = false;


    private float startTime = 0;
    private float endTime = 0;

    public float maxTime;
    public float minSwipeDist;
    float tTime;

    private Vector3 StartPos;
    private Vector3 endPos;


    //For tutorial
    public bool takeInputleft = true;
    public bool takeInputRight = true;
    public bool takeInputCentre = true;
    public bool takeInputDown = false;

    [HideInInspector]
    public bool isCleared = false;

    private void Start()
    {
        SetPointerHighlight();
        startTime = Time.time;
        fallSpeed = GameManager.instance.FallSpeed;
    }
    private void Update()
    {
        CheckInput();

    }

    private bool firstClick = false;
    private void CheckInput()
    {

        if (!controlPiece)
        {
            if (!this.pieceSettled)
            {
                this.pieceSettled = true;
                pointer_Highlight.transform.position = new Vector3(7.6f, pointer_Highlight.transform.position.y, pointer_Highlight.transform.position.z);
                GameObject[] go = new GameObject[4];
                GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
                if (CheckforSameColorBlock())
                {
                    ScoreManager.instance.UpdateScore();
                }
                for (int i = transform.childCount - 1; i >= 0; i--)
                {
                    transform.GetChild(i).SetParent(holder.transform);
                }
                FindObjectOfType<GridSystem>().SettlePeicesinEmpty();
                FindObjectOfType<GridSystem>().FinishClearing();

                GameObject.Destroy(gameObject, 2f);
            }
            return;
        }


        if (Time.time - fall >= fallSpeed)
        {
            makeMove = true;
            if (makeMove)
            {
                /*  endTime = startTime;
                  startTime = Time.time;
                  if (Time.time - endTime < 0.2f)
                  {
                      fallSpeed = 0;
                      endTime += 10;
                  }*/

                transform.position += new Vector3(0, -moveOffset, 0);
                this.controlPiece = false;
                if (CheckIfValid())
                {
                    //   GridSystem.instance.UpdateGrid(this);

                    this.controlPiece = true;
                }
                else
                {

                    transform.position += new Vector3(0, moveOffset, 0);                  //To cancel out, out of index array in case the piece is out of the grid

                    GridSystem.instance.UpdateGrid(this);

                    if (CheckForStackOverFlow())
                    {
                        ScoreManager.instance.SaveScore();
                        AudioHandler.Instance.FadeAudio();
                        GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
                        GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
                        FindObjectOfType<TrailHead>().StopAllCoroutines();
                        Invoke("ShowResult", 2f);
                        return;
                    }

                    //   this.controlPiece = false;
                    pointer_Highlight.transform.position = new Vector3(7.6f, pointer_Highlight.transform.position.y, pointer_Highlight.transform.position.z);
                    FindObjectOfType<SpawnMechanic>().spawnPieces.RemoveAt(0);
                    FindObjectOfType<SpawnMechanic>().SpawnPiece();
                }
            }
            fall = Time.time;

        }


        if (Input.GetMouseButtonDown(0))
        {
            startTime = Time.time;
            StartPos = Input.mousePosition;

            firstClick = true;
        }
        else if(Input.GetMouseButton(0) && GameManager.instance.isSwipingEnabled)
        {
            float swipeTime = Time.time - startTime;
            float swipeDistance = (Input.mousePosition - StartPos).magnitude;

            if (firstClick)
            {
                firstClick = false;
                swipeTime = maxTime + 5f;
                swipeDistance = minSwipeDist + 0;
            }
            if (swipeTime > maxTime && swipeDistance > minSwipeDist)   //minSwipeDist * 1.5f
            {
                Vector2 distance = Input.mousePosition - StartPos;
                if (distance.x > 0)
                {
                    //Right Swipe
                    //  StartCoroutine(ExecuteCommandBlockCoroutine(0));
                    ExecuteCommandBlock(0);
                    StartPos = Input.mousePosition;
                }
                else if (distance.x < 0)
                {
                    //Left Swipe
                  //  StartCoroutine(ExecuteCommandBlockCoroutine(1));
                    ExecuteCommandBlock(1);
                    StartPos = Input.mousePosition;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            endTime = Time.time;
            endPos = Input.mousePosition;
            float swipeDistance = (endPos - StartPos).magnitude;
            float swipeTime = endTime - startTime;

            if (swipeTime < maxTime && swipeDistance > minSwipeDist)
            {
                Vector2 distance = endPos - StartPos;

                if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y) && GameManager.instance.isSwipingEnabled)
                {
                    if (distance.x > 0)
                    {
                        //Right Swipe
                        ExecuteCommandBlock(0);
                    }
                    else if (distance.x < 0)
                    {
                        //Left Swipe
                        ExecuteCommandBlock(1);
                    }
                }
                else if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
                {
                    if (distance.y < 0)
                    {
                        //Down Swipe
                        ExecuteCommandBlock(2);
                    }
                }
            }
            else if(swipeTime < maxTime && swipeDistance < minSwipeDist)
            {
                ExecuteCommandBlock(3);
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ExecuteCommandBlock(2);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            startTime = Time.time;
       //     GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            ExecuteCommandBlock(0);

            tTime = startTime + 0.15f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            float swipeTime = Time.time - startTime;

            if (Time.time > tTime)
            {
                ExecuteCommandBlock(0);
                startTime = Time.time;
                tTime = startTime + 0.15f;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            startTime = Time.time;
          //  GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            ExecuteCommandBlock(1);

            tTime = startTime + 0.15f;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (Time.time > tTime)
            {
                ExecuteCommandBlock(1);
                startTime = Time.time;
                tTime = startTime + 0.15f;
            }
        }
        else if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            StopCoroutine(ExecuteCommandBlockCoroutine(-1));
        }
        else if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Submit"))
        {
        //    GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            ExecuteCommandBlock(3);
        }

       
    }

    private void ShowResult()
    {
        UIHandler.instance.ShowResult();


        //Jio_AdsManager.Instance.Jio_MID_Roll.showAd();
        //Jio_AdsManager.Instance.Jio_MID_Roll.cacheAd();
    }
    public void ExecuteCommandBlock(int i)
    {
        switch (i)
        {
            case 0:
                if (!takeInputRight)
                    return;
                //Right Side
                makeMove = true;
                transform.position += new Vector3(moveOffset, 0, 0);

                if (CheckIfValid())
                {
                    //  GridSystem.instance.UpdateGrid(this);
                }
                else
                {
                    transform.position += new Vector3(-moveOffset, 0, 0);
                    GridSystem.instance.UpdateGrid(this);
                }
                break;
            case 1:

                if (!takeInputleft)
                    return;
                //Left Side
                makeMove = true;
                transform.position += new Vector3(-moveOffset, 0, 0);
                if (CheckIfValid())
                {
                    // GridSystem.instance.UpdateGrid(this);
                }
                else
                {
                    transform.position += new Vector3(moveOffset, 0, 0);
                    GridSystem.instance.UpdateGrid(this);
                }
                break;
            case 2:
                //Down Side
                if (!takeInputDown)
                    return;
                fallSpeed = 0;
                break;
            case 3:
                if (!takeInputCentre)
                    return;
                //Flip
                transform.Rotate(0, 0, -90);
                makeMove = true;
                break;
            default:
                break;
        }
        pointer_Highlight.transform.position = new Vector3(transform.position.x, pointer_Highlight.transform.position.y, pointer_Highlight.transform.position.z);
    }

    public IEnumerator ExecuteCommandBlockCoroutine(int i)
    {
        yield return new WaitForSeconds(0.25f);

        switch (i)
        {
            case 0:
                //Right Side
                makeMove = true;
                transform.position += new Vector3(moveOffset, 0, 0);

                if (CheckIfValid())
                {
                    //  GridSystem.instance.UpdateGrid(this);
                }
                else
                {
                    transform.position += new Vector3(-moveOffset, 0, 0);
                    GridSystem.instance.UpdateGrid(this);
                }
                break;
            case 1:
                //Left Side
                makeMove = true;
                transform.position += new Vector3(-moveOffset, 0, 0);
                if (CheckIfValid())
                {
                    // GridSystem.instance.UpdateGrid(this);
                }
                else
                {
                    transform.position += new Vector3(moveOffset, 0, 0);
                    GridSystem.instance.UpdateGrid(this);
                }
                break;
            case 2:
                //Down Side
                fallSpeed = 0;
                break;
            case 3:
                //Flip
                transform.Rotate(0, 0, -90);
                makeMove = true;
                break;
            default:
                break;
        }
        pointer_Highlight.transform.position = new Vector3(transform.position.x, pointer_Highlight.transform.position.y, pointer_Highlight.transform.position.z);
        StopCoroutine(ExecuteCommandBlockCoroutine(-1));
    }
    private bool CheckIfValid()
    {
        foreach (Transform square in transform)
        {
            Vector2 pos = FindObjectOfType<GridSystem>().Round(square.position);

            if (FindObjectOfType<GridSystem>().CheckIfInsideGrid(pos) == false)
            {
                return false;
            }

            if (GridSystem.instance.GetTransformAtGridPosition(pos) != null && GridSystem.instance.GetTransformAtGridPosition(pos).parent != transform)
            {
                return false;
            }
        }

        return true;
    }

    public int ReturnChildCount()
    {
        return transform.childCount;
    }

    public Vector3 ReturnAngle()
    {
        return transform.eulerAngles;
    }
    private bool CheckforSameColorBlock()
    {
        if (transform.childCount == 4)
        {
            if (transform.GetChild(0).transform.tag == transform.GetChild(1).transform.tag
                && transform.GetChild(1).transform.tag == transform.GetChild(2).transform.tag
                && transform.GetChild(2).transform.tag == transform.GetChild(3).transform.tag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if(transform.childCount == 2)
        {
            if (transform.GetChild(0).transform.tag == transform.GetChild(1).transform.tag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }
    private bool CheckForStackOverFlow()
    {
        foreach(Transform t in transform)
        {
            Vector2 pos = FindObjectOfType<GridSystem>().Round(t.position);

            if (pos.y >= GridSystem.instance.height + 0.5f)
                return true;
        }

        return false;
    }

    private void SetPointerHighlight()
    {
        holder = GameObject.FindGameObjectWithTag("H");
        pointer_Highlight = GameObject.FindGameObjectWithTag("PH");
        anchor = pointer_Highlight.transform.GetChild(0).gameObject;
        anchor.GetComponent<SpriteRenderer>().enabled = pointer_Highlight.GetComponent<SpriteRenderer>().enabled = true;
        GameObject obj = GameManager.instance.currentPiece;
        pointer_Highlight.transform.position = new Vector3(obj.transform.position.x, pointer_Highlight.transform.position.y, pointer_Highlight.transform.position.z);

    }

   /* public List<int> pieceNumber;
    public List<GameObject> pieceExtra;
    public void SettlePeicesinEmpty()
    {
        pieceNumber = new List<int>();
        pieceExtra = new List<GameObject>();
        Transform[,] grid;
        for (int j = transform.childCount - 1; j >= 0; j--)
        {
  
            GameObject square = transform.GetChild(j).gameObject;
            grid = FindObjectOfType<GridSystem>().ReturnGrid();
            Vector2 pos = FindObjectOfType<GridSystem>().Round(square.transform.position);
            int x = (int)(pos.x);
            int y = (int)(pos.y);
            if (GridSystem.instance.CheckIfInsideGrid(square.transform.position));
            {
                for (int i = y - 1; i >= 0; i--)
                {
                    if (grid[x, i] == null)
                    {
                        pieceNumber.Add(j);
                        GameObject go = Instantiate(square, square.transform.position, square.transform.rotation);
                        pieceExtra.Add(go);
                        square.GetComponent<SpriteRenderer>().enabled = false;
                        if (x == 0 && grid[x + 1, i] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, i].position.x - 1, grid[x + 1, i].position.y);
                        }
                        else if (x == 0 && grid[x + 1, i] == null || (grid[x + 1, i] == null && grid[x - 1, i] == null) || (x == 15 && grid[x - 1, i] == null))
                        {
                            square.transform.position = new Vector2(x * moveOffset, (i * moveOffset));
                        }
                        else if (x == 15 && grid[x - 1, i] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, i].position.x - 1, grid[x - 1, i].position.y);
                        }
                        else if (grid[x + 1, i] != null)
                        {
                            square.transform.position = new Vector2(grid[x + 1, i].position.x - 1, grid[x + 1, i].position.y);
                        }
                        else if (grid[x - 1, i] != null)
                        {
                            square.transform.position = new Vector2(grid[x - 1, i].position.x + 1, grid[x - 1, i].position.y);
                        }
                        else
                        {
                            Debug.Log("None of them is being Executed");
                        }
                    
                        x = (int)pos.x;
                        y = (int)pos.y;
                        FindObjectOfType<GridSystem>().UpdateGrid(this);
                    }
                    else
                    {
                        Debug.Log("Calling");
                        StartCoroutine(reEnableSprite(i));
                        break;
                    }
                }
            }
        }
        EnableTransitions();


       // GameManager.instance.SettleAllPiecesInAllParents();
    }

    private void EnableTransitions()
    {

        for (int i = 0; i < pieceExtra.Count; i++)
        {
            StartCoroutine(GameManager.instance.TransitToPosition(pieceExtra[i], transform.GetChild(pieceNumber[i]).transform.position));
            StartCoroutine(reEnableSprite(i));
        }
    }

    private IEnumerator reEnableSprite(int t)
    {
        yield return new WaitForSeconds(0.6f);
        if (t < pieceNumber.Count)
        {
            transform.GetChild(pieceNumber[t]).GetComponent<SpriteRenderer>().enabled = true;
            Destroy(pieceExtra[t], 0.7f);
        }
        StartCoroutine(CheckGridForMatch());
    }

    private IEnumerator CheckGridForMatch()
    {
        yield return new WaitForSeconds(0.7f);
        for (int i = 0; i < GridSystem.height; ++i)
            for (int j = 0; j < GridSystem.width; ++j)
            {
                GridSystem.instance.ClearAllMatches(j, i);

            }

        yield return new WaitForSeconds(0.3f);
        //   SettlePeicesinEmpty();
        yield return new WaitForSeconds(0.2f);
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;

    }*/
}

