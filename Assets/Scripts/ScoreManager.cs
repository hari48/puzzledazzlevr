﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    public int score, highScore, cleared, coins;

    public int earnedXp;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        GetSavedScore();
        
    }
    public void SaveScore()
    {
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("hs", highScore);
        }
        coins = GameManager.instance.Coins;
        PlayerPrefs.SetInt("coins", coins);

    }

    public void GetSavedScore()
    {
        score = 0;
        highScore = PlayerPrefs.GetInt("hs", 0);
        PlayerPrefs.GetInt("coins", 0);
    }

    public int ReturnHighScore()
    {
        return PlayerPrefs.GetInt("hs", 0);
    }

    public int ReturnCoins()
    {
        return PlayerPrefs.GetInt("coins", 0);
    }

    public void UpdateScore()
    {
        score += 500;
        cleared++;

        if (score % 1000 == 0)
            GameManager.instance.UpdateCoins(100);

        if(score % 10000 == 0 && score < 40000)
        {
            GameManager.instance.FallSpeed -= 0.08f;
        }

        if (cleared >= FindObjectOfType<LevelUniqueIdentifier>().levelTarget)
            FindObjectOfType<LevelUniqueIdentifier>().targetAchieved = true;

        SaveScore();

        UIHandler.instance.UpdateScoreUI();
    }


}
