﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;

public class Scaler : MonoBehaviour
{
    private Vector3 currentScale;
    private Vector3 cScale;
    private float startScale;
    public float maxVibration = 0.45f;
    public float flashFlicker = 0.2f;

    public float widthScale, heightScale;

    public SpriteRenderer flash;

    public ParticleSystem par;

    public VisualEffect m_VFX;
    public ExposedProperty m_myProperty = "Intensity";

    public SpriteRenderer[] glowingStar;
    public ParticleSystem[] poofParticles;



    void Start()
    {
        float width = ScreenSize.GetScreenToWorldWidth;
        float height = ScreenSize.GetScreenToWorldHeight;
        if(!gameObject.CompareTag("dontscale"))
     //   transform.localScale = new Vector3(width / widthScale, height / heightScale, 1);

        currentScale = transform.localScale;

        for (int i = 0; i < poofParticles.Length; i++)
        {
            //     cScale = glowingStar[i].transform.localScale;
            startScale = poofParticles[i].startSize;
        }
    }

    private void Update()
    {
        AudioScalerForBG();
        FlashFlicker();
        ParticleMusicControl();
    }

    private void AudioScalerForBG()
    {
        float buffer = AudioPeer.amplitudeBuffer;
        buffer = 1 + (buffer / 20);
        transform.localScale = currentScale * (buffer);
        transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x, 0.1f, maxVibration), Mathf.Clamp(transform.localScale.y, 0.1f, maxVibration + 0.2f), Mathf.Clamp(transform.localScale.z, 0.1f, maxVibration));

    }
    private void FlashFlicker()
    {
        float buffer = AudioPeer.audioBandBuffer[6];
   //     buffer = buffer / 9;
        flash.color = new Color(flash.color.r, flash.color.g, flash.color.b, buffer);
        flash.color = new Color(flash.color.r, flash.color.g, flash.color.b, Mathf.Clamp(flash.color.a, 0, flashFlicker));

        buffer = 0.3f + AudioPeer.audioBandBuffer[5];
        for (int i = 0; i < poofParticles.Length; i++)
        {
       //     glowingStar[i].color = new Color(glowingStar[i].color.r, glowingStar[i].color.g, glowingStar[i].color.b, buffer);
       //     glowingStar[i].color = new Color(glowingStar[i].color.r, glowingStar[i].color.g, glowingStar[i].color.b, Mathf.Clamp(glowingStar[i].color.a, 0.45f, 1f));
            //Size
          
            float min = 1f;
            float max = 5f;
            //  glowingStar[i].transform.localScale = cScale * buffer;
            //  glowingStar[i].transform.localScale = new Vector3(Mathf.Clamp(glowingStar[i].transform.localScale.x, min, max), Mathf.Clamp(glowingStar[i].transform.localScale.y, min, max), Mathf.Clamp(glowingStar[i].transform.localScale.z, min, max));
            poofParticles[i].gameObject.SetActive(true);
            poofParticles[i].startSize = startScale * buffer;
      //      poofParticles[i].transform.localScale = new Vector3(Mathf.Clamp(poofParticles[i].transform.localScale.x, min, max), Mathf.Clamp(poofParticles[i].transform.localScale.y, min, max), Mathf.Clamp(poofParticles[i].transform.localScale.z, min, max));

        }
    }

    public void ParticleMusicControl()
    {
        float buffer = AudioPeer.audioBandBuffer[6];
        buffer = buffer / 2;

        m_VFX.SetFloat(m_myProperty,  AudioPeer.amplitudeBuffer);

        var main = par.main;

        main.simulationSpeed = 1 + (main.startSpeed.constant * buffer);
    //    main.startSpeed = 3 + (main.startSpeed.constant * buffer);
    }
}
