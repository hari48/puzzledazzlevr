﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailHead : MonoBehaviour
{
    public GameObject trailHead;
    public float speed = 50f;

    private void Start()
    {
        StartCoroutine(Tail_Travel());
    }


    private IEnumerator Tail_Travel()
    {
        while(trailHead.transform.position.x < GridSystem.instance.width - 0.3f)
        {
            trailHead.transform.Translate(Vector3.right * speed * Time.deltaTime);
            yield return null;
        }
        StopCoroutine(Tail_Travel());
        yield return new WaitForSeconds(0.19f);
        trailHead.transform.gameObject.SetActive(false);
        trailHead.transform.position = new Vector3(-0.5f, trailHead.transform.position.y, 0);
        yield return new WaitForSeconds(0.2f);
        StartCoroutine(Tail_Travel());
        trailHead.transform.gameObject.SetActive(true);

    }

    public void StopThisRoutine()
    {
        StopCoroutine(Tail_Travel());
    }

}
