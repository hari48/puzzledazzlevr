﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VariableHolder : MonoBehaviour
{
    public TextMeshProUGUI text_Challenge1, text_Challenge2, text_Challenge3;
    public List<UnityEngine.UI.Toggle> toggles;

    public Image xp_FillerImage;
    public TextMeshProUGUI levelText, playerName;


    public static VariableHolder instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
}
