﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SurvivalMode : MonoBehaviour
{
    [Header("Time to wait for freezing in seconds")]
    public float timeToWait = 30f;
    private float waitTime;
    public TextMeshProUGUI timerText;

    public float timer;
    public bool isSurvivalMode = false;

    public Material pinkMat;

    private void Awake()
    {
        if(PlayerPrefs.GetInt("isSurvival", 0) == 1)
        {
            isSurvivalMode = true;
        }
        else
        {
            isSurvivalMode = false;
        }
    }

    private void Start()
    {
        timer = timeToWait;
        waitTime = Time.time + timeToWait;
    }

    private void Update()
    {
        if (!isSurvivalMode)
        {
            timerText.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }

        if(timer >= 0)
        {
            timer -= Time.deltaTime;
            timerText.text = "Timer : " + (int)timer;
        }

        if(Time.time > waitTime)
        {


            waitTime = Time.time + (timeToWait + 2);
            Debug.Log("Beep Bop Bup! I just froze some of your blocks!");
            StartCoroutine(FreezeSomeBlocks());
            GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = false;
            //      FreezeSomeBlocks();

        }
    }

    List<GameObject> blocksToFreeze = new List<GameObject>();

    private IEnumerator FreezeSomeBlocks()
    {

        List<int> numbersUsed = new List<int>();
        List<Transform> gridList = new List<Transform>();
        int numberOfblocksToFreeze = Random.Range(4, 6);

        for (int x = 0; x < GridSystem.instance.width; x++)
        {
            for (int y = 0; y < GridSystem.instance.height; y++)
            {
                if (GridSystem.grid[x, y] != null && !blocksToFreeze.Contains(GridSystem.grid[x,y].gameObject))
                {
                    gridList.Add(GridSystem.grid[x, y]);

                }
            }
        }

        //Getting all the grids from 2d array and converting them to list
        for (int i = 0; i < numberOfblocksToFreeze; i++)
        {



            //if there are no blocks at all in grid? just return null and don't freeze anything
            if(gridList.Count <= 0)
            {
                break;
            }

            //Making sure to break loop incase there are not blocks left to freeze
            if (blocksToFreeze.Count == gridList.Count)
                break;

            //if the blocks available in the grid are less than this loop? then reduce the loop count to grid size
            if (gridList.Count <= numberOfblocksToFreeze)
            {
                numberOfblocksToFreeze = gridList.Count;
            }


            //Freezing unique blocks in grid
            int rand = Random.Range(0, gridList.Count);
            if (!blocksToFreeze.Contains(gridList[rand].gameObject))
            {
                blocksToFreeze.Add(gridList[rand].gameObject);
                GameObject go = Instantiate(gridList[rand].gameObject, gridList[rand].gameObject.transform.position, gridList[rand].transform.rotation, gridList[rand].transform);
                go.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
                gridList.RemoveAt(rand);
                go.GetComponent<Renderer>().material = pinkMat;
                yield return new WaitForSeconds(0.3f);
            }
        }

        yield return new WaitForSeconds(2f);

        timer = timeToWait;
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;
    }
}
