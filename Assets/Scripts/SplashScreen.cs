﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    private void OnEnable()
    {
     //   StartCoroutine(LoadwithDelay());
    }

    private IEnumerator LoadwithDelay()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Menu");
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }
}
