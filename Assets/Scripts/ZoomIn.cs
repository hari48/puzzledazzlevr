﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ZoomIn 
{
    public static IEnumerator Zoomin(Vector3 startSize, Vector3 endSize, GameObject parent, float zoomAnimationSpeed)
    {
        while (Vector3.Distance(startSize, endSize) >= 0f)
        {
            parent.transform.localScale = new Vector3(Mathf.Lerp(startSize.x, endSize.x, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.y, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.z, Time.time * zoomAnimationSpeed));
            yield return null;
        }
    }
}
