﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using TMPro;

public enum GameOrientation
{
    landscape = 0,
    portrait = 1
}
 public enum GameMode
{
    Arcade = 0,
    Survival = 1

}
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool isGameover = false;
    public GameOrientation gameOrientation;
    public GameMode gameMode;
    public bool isSurvival = true;


    public bool spawnPiece = false;

    public GameObject currentPiece;

    public GameObject[] pieceControllerObj;

    public float customTimeScale = 1f;

    public TMP_Dropdown dropdown;

    public GameObject controlButtons;
    public bool isSwipingEnabled = false;
    private float fallSpeed = 0.8f;

    public GameObject Effects;

    public float FallSpeed
    {
        get
        {
            return fallSpeed;
        }
        set
        {
            fallSpeed = value;
        }
    }

    private int coins = 0;

    public int Coins { get { return coins; } set { coins = value; } }

    private void Start()
    {
   //     Time.timeScale = customTimeScale;
        UIHandler.instance.UpdateScoreUI();
        coins = PlayerPrefs.GetInt("coins", 0);

        UIHandler.instance.UpdateScoreUI();
        ControlDropDown();


        //     StartCoroutine(DelayEffects());


    }

    private IEnumerator DelayEffects()
    {
        yield return new WaitForSeconds(1f);
        Effects.SetActive(true);
    }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIHandler.instance.OnPressedPause();
        }


        if (Input.GetKeyDown(KeyCode.Z))
        {
            FindObjectOfType<AudioHandler>().mainAudioSource.time = FindObjectOfType<AudioHandler>().mainAudioSource.clip.length - 10;
        }

        if (FindObjectOfType<LevelUniqueIdentifier>().songcompleted && FindObjectOfType<LevelUniqueIdentifier>().targetAchieved)
        {
            int level = SceneManager.GetActiveScene().buildIndex;

            level++;

            if (level >= SceneManager.sceneCountInBuildSettings)
                level = 3;

            Debug.LogError("SF " + SceneManager.sceneCount);
            SceneManager.LoadScene(level);
        }
        else if(FindObjectOfType<LevelUniqueIdentifier>().songcompleted && !FindObjectOfType<LevelUniqueIdentifier>().targetAchieved)
        {
            ScoreManager.instance.SaveScore();
            AudioHandler.Instance.FadeAudio();
            GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
            GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            FindObjectOfType<TrailHead>().StopAllCoroutines();
            UIHandler.instance.ShowResult();
        }
    }

    private void ShowResult()
    {
        //UIHandler.instance.ShowResult();
        Instantiate(Resources.Load("Effects") as GameObject);
    }

    public void OnPressedLeft()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(1);
    }

    public void OnPressedRight()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(0);
    }

    public void OnPressedFlip()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(3);
    }

    public void ControlDropDown()
    {
        if(dropdown.value == 0)
        {
            isSwipingEnabled = true;
            controlButtons.SetActive(false);
        }
        else if(dropdown.value == 1)
        {
            isSwipingEnabled = false;
            controlButtons.SetActive(true);
        }
    }

    public void UpdateCoins(int amount)
    {
        Coins += amount;
        UIHandler.instance.UpdateScoreUI();
    }

    private void SetGameMode()
    {
        switch(PlayerPrefs.GetInt("gamemode"))
        {
            case 0:
                gameMode = GameMode.Arcade;
                break;
            case 1:
                gameMode = GameMode.Survival;
                break;
            default:
                break;
        }
    }
}
