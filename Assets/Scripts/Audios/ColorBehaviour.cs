﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBehaviour : MonoBehaviour
{
    public GameObject target;
    public float times = 5f;
    public float roughness;
    private void Start()
    {
    }
    int i = 0;
    private void Update()
    {
     //   camShaker.ShakeOnce(AudioPeer.audioBandBuffer[5] * times, AudioPeer.audioBandBuffer[6] * times, 0, 0);
     //   camShaker.StartShake(AudioPeer.audioBandBuffer[5] * times, roughness, 0.15f);
        if (Input.GetKeyUp(KeyCode.E))
        {
            i++;
            if (i > 7)
            {
                i = 0;
            }
        }

        if (AudioPeer.audioBandBuffer[i] > 0.5)
        {
            target.GetComponent<MeshRenderer>().material.color = Color.blue;
            target.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.blue);
        }
        else if (AudioPeer.audioBandBuffer[i] > 0.2 && AudioPeer.audioBandBuffer[i] < 0.5)
        {
            target.GetComponent<MeshRenderer>().material.color = Color.red;
            target.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red);
        }
        else
        {
            target.GetComponent<MeshRenderer>().material.color = Color.white;
            target.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.blue);
        }
    }
}
