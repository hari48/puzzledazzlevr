﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPeer : MonoBehaviour
{
    AudioSource audioSource;
    public static float[] samples = new float[512];
    public static float[] frequencyBand = new float[8];
    public static float[] bandBuffer = new float[8];
    private float[] bufferDecrease = new float[8];

    public float[] freqBandHighest = new float[8];
    public static float[] audioBand = new float[8];
    public static float[] audioBandBuffer = new float[8];
    public static float amplitude, amplitudeBuffer;

    private float amplitudeHeighest;

    public float a = 0.005f;
    public float b = 1.2f;

    public float _audioProfile;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        AudioProfile(_audioProfile);
    }

    int i = 0;
    private void Update()
    {
        GetSpectrumAudioSource();
        MakeFrequencyBands();
        BandBuffer();
        CreateAudioBands();
        GetAmplitude();

    /*    Debug.Log("Band " + i + " = " + audioBandBuffer[i]);
        if (Input.GetKeyUp(KeyCode.E))
        {
            if(i > 7)
            {
                i = 0;
            }
            i++;
        }*/
    }

    private void AudioProfile(float audioP)
    {
        for (int g = 0; g < 8; ++g)
        {
            freqBandHighest[g] = audioP;
        }
    }

    private void GetAmplitude()
    {
        float currentAmplitude = 0;
        float currentAmplitudeBuffer = 0;

        for(int i = 0; i < 8; i++)
        {
            currentAmplitude += audioBand[i];
            currentAmplitudeBuffer += audioBandBuffer[i];
        }

        if(currentAmplitude > amplitudeHeighest)
        {
            amplitudeHeighest = currentAmplitude;
        }

        amplitude = currentAmplitude / amplitudeHeighest;
        amplitudeBuffer = currentAmplitudeBuffer / amplitudeHeighest;
    }

    private void GetSpectrumAudioSource()
    {
        audioSource.GetSpectrumData(samples, 0, FFTWindow.Blackman);
    }

    private void MakeFrequencyBands()
    {
        /*
         *  22050 / 512  = 43hertz per sample
         *  
         *  20 - 60 hertz
         *  250 - 500 hertz
         *  500 - 2000 hertz
         *  2000 - 4000 hertz
         *  4000 - 6000 hertz 
         *  6000 - 20000 hertz
         *  
         *  0 - 2 = 86 hertz
         *  1 - 4 = 172 hertz - 87 - 258
         *  2 - 8 = 344 hertz - 259 - 602
         *  3 - 16 = 688 hertz - 603 - 1290
         *  4 - 32 = 1376 hertz - 1201 - 2666
         *  5 - 64 = 2752 hertz - 2667 - 5418 
         *  6 - 128 = 5504 hertz - 5419 - 10922
         *  7 - 256 = 11008 hertz - 10923 - 21930
         *  512
        */

        int count = 0;
        for(int i = 0; i < 8; i++)
        {
            float average = 0;
            int sampleCount = (int)Mathf.Pow(2, i) * 2;

            if(i == 7)
            {
                sampleCount += 2;
            }

            for(int j = 0; j < sampleCount; j++)
            {
                average += samples[count] * (count + 1);
                count++;
            }

            average /= count;
            frequencyBand[i] = average * 10;
        }
    }

    private void BandBuffer()
    {
        for(int g = 0; g < 8; ++g)
        {
            if(frequencyBand[g] > bandBuffer[g])
            {
                bandBuffer[g] = frequencyBand[g];
                bufferDecrease[g] = a;

            }

            if (frequencyBand[g] < bandBuffer[g])
            {
                bandBuffer[g] -= bufferDecrease[g];
                bufferDecrease[g] *= b;
            }
        }
    }

    private void CreateAudioBands()
    {
        for(int i = 0; i < 8; i++)
        {
            if(frequencyBand[i] > freqBandHighest[i])
            {
                freqBandHighest[i] = frequencyBand[i];
            }

            audioBand[i] = (frequencyBand[i] / freqBandHighest[i]);
            audioBandBuffer[i] = (bandBuffer[i] / freqBandHighest[i]);

        }
    }
}
