﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using AudioVisualizer;

public class AudioHandler : MonoBehaviour
{
    public AudioClip[] audioClips;
    public AudioSource mainAudioSource;
    public List<AudioClip> audios = new List<AudioClip>();
    public bool isPaused = false;

    public AudioClip gameOverClip;
    private static AudioHandler instance;

    public GameObject songInfo;
    private string[] songDetails;


    public static AudioHandler Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AudioHandler>();
            }

            return instance;
        }
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<AudioHandler>();
        }
       else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;

    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    private void Start()
    {
        
        mainAudioSource = this.GetComponent<AudioSource>();

      
 /*       ResetAudioFiles();
        PlayRandomAudio();*/
    }

    private void Update()
    {
        if(mainAudioSource != null)
        if (!mainAudioSource.isPlaying && !isPaused && mainAudioSource.enabled)
        {
            PlayRandomAudio();
                FindObjectOfType<LevelUniqueIdentifier>().songcompleted = true;
        }
    }

    public void PlayRandomAudio()
    {
        if (songInfo != null)
            songInfo.SetActive(false);
        int rand = Random.Range(0, audios.Count);
        mainAudioSource.time = 0;
        if (audios.Count == 0)
        {
            ResetAudioFiles();
        }
        mainAudioSource.clip = audios[rand];
    //    if(SceneManager.GetActiveScene().name == "Level" || SceneManager.GetActiveScene().name == "Level_Portrait")
    //    {
            if(audios[rand].name == "264 Raven Link & Vin Bogart - Bleeding")
            StartCoroutine(FadeSongInfo(0));
            else if (audios[rand].name == "ROY - Lost In Sound(Magic Release)")
                StartCoroutine(FadeSongInfo(1));
            else if (audios[rand].name == "ROY KNOX - Firefly")
                StartCoroutine(FadeSongInfo(2));
            else if (audios[rand].name == "The FifthGuys & Coffeeshop - She Makes Me (ft H3R0) [Gaming Playlist Co-release]")
                StartCoroutine(FadeSongInfo(3));
    //    }
        audios.RemoveAt(rand);
        mainAudioSource.Play();

    }

    public void ResetAudioFiles()
    {
        audios.Clear();
        for (int i = 0; i < audioClips.Length; i++)
        {
            audios.Add(audioClips[i]);
        }
    }

    public void InitializeAudioSource()
    {
        //        mainAudioSource = FindObjectOfType<AudioSource>();
        mainAudioSource = this.GetComponent<AudioSource>();
        mainAudioSource.volume = 0.6f;

        if(PlayerPrefs.GetInt("isSurvival") == 0 && SceneManager.GetActiveScene().buildIndex >= 3)
        {
            audioClips = null;
            audioClips = new AudioClip[FindObjectOfType<LevelUniqueIdentifier>().clipstobeplayed.Length];
            audioClips = FindObjectOfType<LevelUniqueIdentifier>().clipstobeplayed;
        }
        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            mainAudioSource.enabled = true;
        }
        else
        {
            mainAudioSource.enabled = false;
        }

        songDetails = new string[4];
        songDetails[0] = "Bleeding\nRaven Link & Vin Bogart\nMagic Records";
        songDetails[1] = "Lost In Sound\nRoy Knox\nMagic Records";
        songDetails[2] = "FireFly\nRoy Knox\nMagic Records";
        songDetails[3] = "She Makes Me (ft H3R0)\nThe FifthGuys & Coffeeshop\nMagic Records";
        songInfo = GameObject.FindGameObjectWithTag("SongInfo");
        ResetAudioFiles();
        PlayRandomAudio();
        //    PlayRandomAudio();
    }

    public void FadeAudio()
    {
       StartCoroutine(AudioFadeOut.FadeOut(mainAudioSource, 0.5f));
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
      //  Debug.Log("OnSceneLoaded: " + scene.name);

        AudioHandler.Instance.InitializeAudioSource();
        
    }


    public IEnumerator FadeSongInfo(int num)
    {
        if (songInfo == null)
            yield return null;
        songInfo.SetActive(true);
        songInfo.GetComponentInChildren<TextMeshProUGUI>().text = songDetails[num];
        yield return null;
    }
}
