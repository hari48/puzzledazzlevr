﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMechanic : MonoBehaviour
{
    public List<GameObject> spawnPieces;
    public Transform[] ExtraPiecesTransform;
    public Sprite[] piece_Tex;
    public Material[] mats;
    public Sprite[] currentPieceTex;

    private void Start()
    {
        int index = PlayerPrefs.GetInt("theme", 0);
        currentPieceTex = new Sprite[2];
        currentPieceTex[0] = piece_Tex[index * 2];
        currentPieceTex[1] = piece_Tex[(index * 2) + 1];
        spawnPieces = new List<GameObject>();

        for (int i = 0; i < 4; i++)
        {
            SpawnPiece();
            if (i > 0)
                spawnPieces[i].GetComponent<PieceController>().enabled = false;
        }
        ReArrangeExtraPieces();
    }


    public void SpawnPiece()
    {
        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);
        }
        GameObject go;
        if (GameManager.instance.gameOrientation == GameOrientation.landscape)
            go = Instantiate(Resources.Load("Piece_Square") as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.65f, GridSystem.instance.height + 0.7f, 0), Quaternion.identity, transform);
        else
            go = Instantiate(Resources.Load("Piece_Square") as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0), Quaternion.identity, transform);
        go.GetComponent<PieceController>().enabled = false;
        SetPiece(go);
        spawnPieces.Add(go);
        if (spawnPieces.Count == 4)
            ReArrangeExtraPieces();
        GameManager.instance.spawnPiece = false;

    }

    public void ReArrangeExtraPieces()
    {
        for(int i = 1; i < 4; i++)
        {
            spawnPieces[i].transform.position = ExtraPiecesTransform[i - 1].position;
        }
    }

    private void SetPiece(GameObject go)
    {
        int randomP = Random.Range(0, 2);
        if (randomP == 1)
        {
            foreach (Transform t in go.transform)
            {
                int rand = Random.Range(0, 2);
            //    t.GetComponent<SpriteRenderer>().sprite = currentPieceTex[rand];
                t.GetComponent<Renderer>().material = mats[rand];
                if (rand == 0)
                {
                    t.tag = "Blue";
                }
                else
                {
                    t.tag = "Yellow";
                }
            }
        }
        else
        {
            GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
            GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 2).gameObject);
            foreach (Transform t in go.transform)
            {
                int rand = Random.Range(0, 2);
                //       t.GetComponent<SpriteRenderer>().sprite = currentPieceTex[rand];
                t.GetComponent<Renderer>().material = mats[rand];
                if (rand == 0)
                {
                    t.tag = "Blue";
                }
                else
                {
                    t.tag = "Yellow";
                }
            }
        }
    }
}
