﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialMobile : MonoBehaviour
{
    public PieceController piece;
    public Sprite blue_sprite, yellow_sprite;
    public GameObject HighlightSquare;
    public TextMeshProUGUI text;
    private bool left, right, centre = false;
    private bool part1 = true, part2 = false;
    private bool secondblocksetup = false;

    private int isFirstTime = 0;

    public bool isPotrait = true;

    //Landscapepositions
    private float firstYstop = 8.5f, firstYstop_2 = 9f;
    private float firstXstop = 14.5f, firstXstop_2 = 15f;
    private Vector2 highlightSquarePosition_1 = new Vector2(14.76f, 8.64f), highlightSquarePosition_2 = new Vector3(7.64f, 8.64f, 0), highlightSquarePosition_3 = new Vector3(5.61f, 8.64f);
    private float secondXstop = 8f;
    // private float thirdYstop = 3.5f, thirdYstop_1 = 4f;
    private float thirdYstop = 7.5f, thirdYstop_1 = 8f;
    private float thirdXstop = 5.5f, thirdXstop_1 = 6f;

    Vector2 startPos;
    float startTime;
    float endTime;
    Vector2 endPos;
    float swipeDistance;
    float swipeTime;
    Vector2 StartPos;


    private void Awake()
    {
        PlayerPrefs.DeleteAll();
        isFirstTime = PlayerPrefs.GetInt("firsttime", 0);
        if (isFirstTime == 0)
        {
            PlayerPrefs.SetInt("firsttime", 1);
        }
        else
        {
            DeleteTutorial();
        }

        if (isPotrait)
        {
            firstYstop = 14.5f;
            firstYstop_2 = 15f;
            highlightSquarePosition_1 = new Vector2(8.67f, 14.76f);
            highlightSquarePosition_2 = new Vector2(4.67f, 14.76f);
            highlightSquarePosition_3 = new Vector2(2.52f, 14.76f);
            firstXstop = 8.5f;
            firstXstop_2 = 9f;
            secondXstop = 5f;
            //    thirdYstop = 8.5f;
            //     thirdYstop_1 = 9f;
            thirdYstop = 13.5f;
            thirdYstop_1 = 14f;
            thirdXstop = 2.5f;
            thirdXstop_1 = 3;
            text.gameObject.transform.localScale = Vector2.one;
        }
        else
        {
            thirdYstop = 6.5f; thirdYstop_1 = 7f;
        }
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.05f);
        HighlightSquare.transform.position = highlightSquarePosition_1;
        piece = GameManager.instance.currentPiece.GetComponent<PieceController>();
        piece.takeInputleft = piece.takeInputRight = false;
        piece.takeInputCentre = false;
        piece.takeInputDown = false;
        right = true;
        SetupBlock(1);
    }

    private void Update()
    {
        
    }

    private void DeleteTutorial()
    {
        GameObject.Destroy(HighlightSquare);
        GameObject.Destroy(text);
        GameObject.Destroy(this.gameObject);
    }

    int times = 0;
    private IEnumerator getNextPiece()
    {
        yield return new WaitForSeconds(0.2f);


        piece = GameManager.instance.currentPiece.GetComponent<PieceController>();
        piece.takeInputleft = piece.takeInputRight = false;
        piece.takeInputCentre = true;

        piece.takeInputDown = false;
        centre = false;
        right = false;
        left = true;
        SetupBlock(1);
    }

    private void SetupBlock(int tutorialLevel)
    {
        switch (tutorialLevel)
        {
            case 1:
                piece.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = blue_sprite;
                piece.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = yellow_sprite;
                piece.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = blue_sprite;
                piece.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = yellow_sprite;
                piece.transform.GetChild(0).tag = "Blue";
                piece.transform.GetChild(2).tag = "Yellow";
                piece.transform.GetChild(1).tag = "Blue";
                piece.transform.GetChild(3).tag = "Yellow";
                break;
            default:
                break;
        }
    }
}
