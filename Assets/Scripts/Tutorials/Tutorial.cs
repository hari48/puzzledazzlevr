﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutorial : MonoBehaviour
{
    public PieceController piece;
    public Sprite blue_sprite, yellow_sprite;
    public GameObject HighlightSquare;
    public TextMeshProUGUI text;
    private bool left, right, centre = false;
    private bool part1 = true, part2 = false;
    private bool secondblocksetup = false;

    private int isFirstTime = 0;

    public bool isPotrait = true;

    //Landscapepositions
    private float firstYstop = 8.5f, firstYstop_2 = 9f;
    private float firstXstop = 14.5f, firstXstop_2 = 15f;
    private Vector2 highlightSquarePosition_1 = new Vector2(14.76f, 8.64f), highlightSquarePosition_2 = new Vector3(7.64f, 8.64f, 0), highlightSquarePosition_3 = new Vector3(5.61f, 8.64f);
    private float secondXstop = 8f;
    // private float thirdYstop = 3.5f, thirdYstop_1 = 4f;
    private float thirdYstop = 7.5f, thirdYstop_1 = 8f;
    private float thirdXstop = 5.5f, thirdXstop_1 = 6f;

    Vector2 startPos;
    float startTime;
    float endTime;
    Vector2 endPos;
    float swipeDistance;
    float swipeTime;
    Vector2 StartPos;

    private void Awake()
    {

           isFirstTime = PlayerPrefs.GetInt("firsttimetutorial", 0);
           if(isFirstTime == 0)
           {
               PlayerPrefs.SetInt("firsttimetutorial", 1);
           }
           else
           {
               DeleteTutorial();
           }

        if (isPotrait)
        {
            firstYstop = 14.5f;
            firstYstop_2 = 15f;
            highlightSquarePosition_1 = new Vector2(8.67f, 14.76f);
            highlightSquarePosition_2 = new Vector2(4.67f, 14.76f);
            highlightSquarePosition_3 = new Vector2(2.52f, 14.76f);
            firstXstop = 8.5f;
            firstXstop_2 = 9f;
            secondXstop = 5f;
            //    thirdYstop = 8.5f;
            //     thirdYstop_1 = 9f;
            thirdYstop = 13.5f;
            thirdYstop_1 = 14f;
            thirdXstop = 2.5f;
            thirdXstop_1 = 3;
            text.gameObject.transform.localScale = Vector2.one;
        }
        else
        {
            thirdYstop = 6.5f; thirdYstop_1 = 7f;
        }
    }
    IEnumerator Start()
    {

        yield return new WaitForSeconds(0.05f);
        HighlightSquare.transform.position = highlightSquarePosition_1;
        piece = GameManager.instance.currentPiece.GetComponent<PieceController>();
        piece.takeInputleft = piece.takeInputRight = false;
        piece.takeInputCentre = false;
        piece.takeInputDown = false;
        right = true;
        SetupBlock(1);
    }

    private void Update()
    {
        if (part1 == true && part2 == false)
        {
            if (piece != null)
                if (piece.transform.position.y > firstYstop && piece.transform.position.y < firstYstop_2 && right)
                {
                    HighlightSquare.SetActive(true);
                    text.gameObject.SetActive(true);
                    piece.fallSpeed = Mathf.Infinity;
                    piece.takeInputRight = true;
                    text.text = "Swipe right to move the piece to the Right.";
                    if (piece.transform.position.x > firstXstop && piece.transform.position.x < firstXstop_2)
                    {
                        text.text = "Swipe left to move the piece to the left.";
                        piece.takeInputRight = false;
                        piece.takeInputleft = true;
                        HighlightSquare.transform.position = highlightSquarePosition_2;
                        left = true;
                        right = false;
                        piece.takeInputRight = false;
                    }
                }

            if (left)
            {
                if (piece.transform.position.x < secondXstop)
                {
                    if (piece.transform.position.y > firstYstop && piece.transform.position.y < firstYstop_2)
                        piece.takeInputCentre = true;
                    else
                        piece.takeInputCentre = false;
                    //  piece.transform.position = new Vector2(7.6f, 7.6f);
                    piece.takeInputleft = false;
                    text.text = "Tap to flip the puzzle pieces.";
     
                    if(Input.GetMouseButtonUp(0))
                    {
                        piece.takeInputCentre = true;
                    }
                    if (!piece.takeInputCentre && Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Space) || Input.GetButtonUp("Submit") || Input.GetMouseButtonDown(0))
                    {
                        piece.fallSpeed = 0.8f;
                        piece.takeInputCentre = false;
                        HighlightSquare.SetActive(false);
                        text.gameObject.SetActive(false);
                        piece.takeInputDown = false;
                        //      thirdYstop = firstYstop;
                        //       thirdYstop_1 = firstYstop_2;

                    }

                    if (!piece.takeInputDown && piece.transform.position.y > thirdYstop && piece.transform.position.y < thirdYstop_1)
                    {
                        piece.fallSpeed = Mathf.Infinity;
                        text.gameObject.SetActive(true);
                        text.text = "Swipe down to make it fall faster";

                    /*    if(Input.GetMouseButtonDown(0))
                        {
                            piece.fallSpeed = 0;
                            text.gameObject.SetActive(false);
                            part1 = false;
                            part2 = true;
                            text.text = "Ohh here we are";
                        }*/
                    
                        if (Input.touchCount == 1)
                        {
                            Touch t1 = Input.GetTouch(0);

                            //startTime = Time.time; Vector2 StartPos = Vector2.zero;
                            if (t1.phase == TouchPhase.Began)
                            {
                                startPos = t1.position;
                                startTime = Time.time;
                                StartPos = Input.mousePosition;
                            }
                            else if (t1.phase == TouchPhase.Ended)
                            {
                                endTime = Time.time;
                                endPos = Input.mousePosition;
                                swipeDistance = (endPos - StartPos).magnitude;
                                swipeTime = endTime - startTime;

                                if (swipeTime < 1f && swipeDistance > 5)
                                {
                                    Vector2 distance = endPos - StartPos;

                                    if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
                                    {
                                       
                                        if (distance.y < 0)
                                        {
                                            //Down Swipe
                                            piece.fallSpeed = 0;
                                            text.gameObject.SetActive(false);
                                            part1 = false;
                                            part2 = true;

                                        }
                                    }
                                }
                            }
                        }
                    /*    if (Input.GetKeyUp(KeyCode.DownArrow))
                        {
                            piece.fallSpeed = 0;
                            text.gameObject.SetActive(false);
                            part1 = false;
                            part2 = true;
                        } */
                    }
                }
            }
        }

        if (part2 == true && part1 == false)
        {
            
            if (secondblocksetup == false && piece != null)
            { 
                secondblocksetup = true;
                StartCoroutine(getNextPiece());              
            }

            if (piece == null)
            {
                secondblocksetup = false;
                return;
            }
            if (piece.transform.position.y > firstYstop && piece.transform.position.y < firstYstop_2 && piece.takeInputCentre)
            {
                piece.fallSpeed = Mathf.Infinity;
                text.gameObject.SetActive(true);
                text.text = "Tap on screen thrice";
                right = false;


                if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Space) || Input.GetButtonUp("Submit") || Input.GetMouseButtonUp(0) && times < 4)
                {
                    ++times;
                    Debug.Log(times);
                    if (times >= 4)
                    {
                        piece.takeInputCentre = false;
                        HighlightSquare.SetActive(true);
                        HighlightSquare.transform.position = highlightSquarePosition_3;
                        text.text = "Move to highlighted position";
                        piece.takeInputleft = true;
                    }
                }
            }


           if (piece.transform.position.x > thirdXstop && piece.transform.position.x < thirdXstop_1  && piece.takeInputleft)
            {
                piece.takeInputleft = false;
                piece.takeInputDown = true;
                text.text = "Swipe Down arrow to make it reach the bottom fast.";

             
            }

            if (Input.GetMouseButtonDown(0) && piece.takeInputDown)
            {
                piece.takeInputDown = false;
                piece.fallSpeed = 0;
                text.text = "Form square pieces to clear the blocks";
                HighlightSquare.SetActive(false);

                Invoke("DeleteTutorial", 1.5f);
            }


        }
    }

    private void DeleteTutorial()
    {
        GameObject.Destroy(HighlightSquare);
        GameObject.Destroy(text.gameObject);
        GameObject.Destroy(this.gameObject);
    }

    int times= 0;
    private IEnumerator getNextPiece()
    {
        yield return new WaitForSeconds(0.2f);


        piece = GameManager.instance.currentPiece.GetComponent<PieceController>();
        piece.takeInputleft = piece.takeInputRight = false;
        piece.takeInputCentre = true;

        piece.takeInputDown = false;
        centre = false;
        right = false;
        left = true;
        SetupBlock(1);
    }

    private void SetupBlock(int tutorialLevel)
    {
        switch(tutorialLevel)
        {
            case 1:
                piece.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = blue_sprite;
                piece.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = yellow_sprite;
                piece.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = blue_sprite;
                piece.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = yellow_sprite;
                piece.transform.GetChild(0).tag = "Blue";
                piece.transform.GetChild(2).tag = "Yellow";
                piece.transform.GetChild(1).tag = "Blue";
                piece.transform.GetChild(3).tag = "Yellow";
                break;
            default:
                break;
        }
    }
}
