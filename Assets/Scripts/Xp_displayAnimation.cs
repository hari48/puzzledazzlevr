﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
using UnityEngine.UI;

public class Xp_displayAnimation : MonoBehaviour
{
    [SerializeField] private Image fillupImage;
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private TextMeshProUGUI xpDisplay;
    [SerializeField] private GameObject parent;
    [SerializeField] private Vector3 endSize;
    [SerializeField] private float zoomAnimationSpeed = 3;
    int player_xp, level;
    private float waitForseconds = 2f;
    private float waitTime;


    public GameObject Result;
    
    private void OnEnable()
    {
       // Time.timeScale = 3f;
        player_xp = PlayerPrefs.GetInt("xp", 0);
        level = PlayerPrefs.GetInt("level", 1);
        UpdateXpStats(0);
    }


    public void UpdateXpStats(int amount)
    {
        player_xp += amount;
        if (amount != 0)
        {
            ScoreManager.instance.coins += 100;
        }
        level = player_xp / 1000;
        if (player_xp >= (level * 1000))
        {
            level = player_xp / 1000;
            level += 1;
        }

        float result = (float)player_xp / (1000 * (float)level);
        levelText.text = string.Format("lvl    {0}", level);
      //  fillupImage.fillAmount = result;

        StartCoroutine(FillerAnimation(0, result));

    }

    public void _EnableResults()
    {
        Result.SetActive(true);
        parent.SetActive(false);
        GameObject.Destroy(parent.gameObject);
    }

 /*   private IEnumerator FillerAnimation(float val, float nextVal)
    {
        float div = nextVal / player_xp;
        while(val <= nextVal)
        {
            waitTime += Time.deltaTime;
   //         fillupImage.fillAmount = Mathf.Lerp(val, nextVal, waitTime / waitForseconds);

            if (ScoreManager.instance.earnedXp >= 0)
            {
                //       yield return new WaitForSeconds(0.00001f);
                yield return null;
                fillupImage.fillAmount += div;
                xpDisplay.text = (ScoreManager.instance.earnedXp--).ToString();
                if (ScoreManager.instance.earnedXp <= 0)
                {
                    xpDisplay.gameObject.SetActive(false);
                    GameObject.Find("Level Up").GetComponent<Animator>().SetTrigger("out");
                }
            }

          

            yield return null;
        }
        Time.timeScale = 1f;
    }*/

    private IEnumerator FillerAnimation(float val, float nextVal)
    {
        float div = nextVal / player_xp;
        float finalEarnedXp;
        while (val <= nextVal)
        {
            waitTime += Time.deltaTime / waitForseconds;
            fillupImage.fillAmount = Mathf.Lerp(val, nextVal, waitTime);
            finalEarnedXp = Mathf.Lerp(ScoreManager.instance.earnedXp, 0, waitTime);
            xpDisplay.text = (((int)finalEarnedXp).ToString());
            if (finalEarnedXp <= 0)
            {
                xpDisplay.gameObject.SetActive(false);
                GameObject.Find("Level Up").GetComponent<Animator>().SetTrigger("out");
            }
        /*    if (ScoreManager.instance.earnedXp >= 0)
            {
                //       yield return new WaitForSeconds(0.00001f);
                yield return null;
                fillupImage.fillAmount += div;
                xpDisplay.text = (ScoreManager.instance.earnedXp--).ToString();
                
            }*/



            yield return null;
        }
        Time.timeScale = 1f;
    }

    private IEnumerator ZoonInAnim(Vector3 startSize, Vector3 endSize)
    {
        while (Vector3.Distance(startSize, endSize) >= 0f)
        {
            parent.transform.localScale = new Vector3(Mathf.Lerp(startSize.x, endSize.x, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.y, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.z, Time.time * zoomAnimationSpeed));
            yield return null;
        }

        UpdateXpStats(0);
        StopCoroutine(ZoonInAnim(endSize, parent.transform.localScale));

    }

    private IEnumerator ZoonOutAnim(Vector3 startSize, Vector3 endSize)
    {
        while (Vector3.Distance(startSize, endSize) <= 1)
        {
            parent.transform.localScale = new Vector3(Mathf.Lerp(startSize.x, endSize.x, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.y, Time.time * zoomAnimationSpeed), Mathf.Lerp(startSize.y, endSize.z, Time.time * zoomAnimationSpeed));
            yield return null;
        }
    }

}
