﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoardControls : MonoBehaviour
{
    public Button[] buttons;

    int currentIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentIndex = 0;
        buttons[currentIndex].transform.parent.GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("b0", 1);
        UpdateLockStatus();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.DownArrow)||Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (buttons[currentIndex].gameObject.name == "Back")
                buttons[currentIndex].GetComponent<Image>().color = Color.white;
            else
            buttons[currentIndex].GetComponent<Image>().enabled = false;

 

            if (currentIndex<buttons.Length-1)
            {
                currentIndex=currentIndex+1;
            }
            //    Debug.Log(currentIndex);
            if (buttons[currentIndex].gameObject.name == "Back")
                buttons[currentIndex].GetComponent<Image>().color = Color.yellow;
            else
            buttons[currentIndex].GetComponent<Image>().enabled = true;



        }
        if(Input.GetKeyDown(KeyCode.UpArrow)||Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (buttons[currentIndex].gameObject.name == "Back")
                buttons[currentIndex].GetComponent<Image>().color = Color.white;
            else
            buttons[currentIndex].GetComponent<Image>().enabled = false;



            if (currentIndex>0)
            {
                currentIndex=currentIndex-1;
            }
            if (buttons[currentIndex].gameObject.name == "Back")
                buttons[currentIndex].GetComponent<Image>().color = Color.yellow;
            else
            buttons[currentIndex].GetComponent<Image>().enabled = true;

        }
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Submit")) //Change it
        {
            buttons[currentIndex].onClick.Invoke();
            if (currentIndex == buttons.Length - 1)
                return;

           
        }
    }

    public void PurchaseItem()
    {
        currentIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.parent.name);
        if (PlayerPrefs.GetInt("b" + currentIndex, 0) == 1)
        {
            PlayerPrefs.SetInt("theme", currentIndex);
            Debug.Log(currentIndex + " Pressed!");

            UpdateLockStatus();
        }
        else if (PlayerPrefs.GetInt("coins", 0) >= 5000)
        {
            PlayerPrefs.SetInt("theme", currentIndex);
            PlayerPrefs.SetInt("b" + currentIndex, 1);
            Debug.Log(currentIndex + " Pressed!");
            UIHandler.instance.UpdateCoins(-5000);
            UpdateLockStatus();
        }
        else
        {
            UIHandler.instance.popup.SetActive(true);
        }
    }

    public void DelayPopupBack()
    {
        Invoke("ClosePopup", 0.1f);
    }

    public void ClosePopup()
    {
        UIHandler.instance.popup.SetActive(false);
    }

    private void UpdateLockStatus()
    {
        int index = PlayerPrefs.GetInt("theme", 0);
        currentIndex = index;
        for (int i = 0; i < buttons.Length; i++)
        {
            int current= PlayerPrefs.GetInt("b" + i, 0);

       //     if (i == buttons.Length - 1)
       //         return;

            if(i == index)
            {
                buttons[i].transform.parent.GetComponent<Image>().enabled = true;
            }
            else
            {
                buttons[i].transform.parent.GetComponent<Image>().enabled = false;
            }
            if (current == 1)
            {
            //    buttons[i].interactable = true;
                buttons[i].GetComponent<Image>().color = Color.white;
            }
            else
            {
           //     buttons[i].interactable = false;
                buttons[i].GetComponent<Image>().color = Color.grey;
            }
        }
    }
}
