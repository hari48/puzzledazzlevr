﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GridSystem : MonoBehaviour
{

    public int width = 16;
    public int height = 10;

    public Color[] color;
    public Sprite blueSprite, yellowSprite;
    public Material blueMat, yellowMat;
    public Color[] currentColors;
    private GameObject particleExplosion;

    public static Transform[,] grid;

    public float[,] gridPositions;
    public static List<int> gridListToClear = new List<int>();

    public static GridSystem instance;

    public GameObject trailHead;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        grid = new Transform[width, height];
        gridPositions = new float[width, height];
    }

    private void Start()
    {
        Time.timeScale = 1f;
        int index = PlayerPrefs.GetInt("theme", 0);
        currentColors = new Color[2];
        currentColors[0] = color[index * 2];
        currentColors[1] = color[(index * 2) + 1];


        trailHead = FindObjectOfType<TrailHead>().trailHead;
        particleExplosion = Resources.Load("parEx") as GameObject;
        StartCoroutine(DestroyLater());

       
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SettlePeicesinEmpty();
        }

    }
    public void UpdateGrid(PieceController piece)
    {
        if (piece != null)
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    if (grid[x, y] != null)
                    {
                        if (grid[x, y].parent == piece.transform)
                        {
                            grid[x, y] = null;
                        }
                    }
                }
            }

        foreach (Transform child in piece.transform)
        {
            Vector2 pos = Round(child.position);
            //   y > 0 && y < 10 && x > 0 && x < 16
            if (pos.y < height && pos.y >= 0 && pos.x >= 0 && pos.x < 16)
            {
                grid[(int)pos.x, (int)pos.y] = child;
            }
        }
    }

    public void UpdateIndividualGrid(Transform child)
    {

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                if (grid[x, y] != null)
                {
                    if (grid[x, y] == child.transform)
                    {
                        grid[x, y] = null;
                    }
                }
            }
        }

        Vector2 pos = Round(child.position);
        if (pos.y < height && pos.y >= 0 && pos.x >= 0 && pos.x < 16)
        {
            grid[(int)pos.x, (int)pos.y] = child;
        }
    }


    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        if (pos.y > height - 1)
        {
            return null;
        }
        else
        {

            return grid[(int)pos.x, (int)pos.y];
        }
    }

    public bool CheckIfInsideGrid(Vector2 pos)
    {

       if(GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnChildCount() == 2)
        {
            if(GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnAngle().z == 90)
            return ((int)pos.x >= 0 && (int)pos.x < width - 1 && (int)pos.y >= 0);
            else if(GameManager.instance.currentPiece.GetComponent<PieceController>().ReturnAngle().z == 270)
                return ((int)pos.x >= 1 && (int)pos.x < width && (int)pos.y >= 0);
        }

        return ((int)pos.x >= 0 && (int)pos.x < width && (int)pos.y >= 0);
    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
        //return new Vector2(Mathf.Floor(pos.x), Mathf.Floor(pos.y));
    }

    public Transform[,] ReturnGrid()
    {
        return grid;
    }

    public TMP_InputField X;
    public TMP_InputField Y;
    public void DisplayGridInformationAsked()
    {
        int x = int.Parse(X.text);
        int y = int.Parse(Y.text);
        Debug.Log("Grid[" + X.text + "][ " + Y.text + "] = " + grid[x, y]);
    }


    public List<GameObject> matchList = new List<GameObject>();
    public void FloodFillAlgorithm(int i, int j, string colorTag)
    {
        //   Debug.Log("i = " + i + " j = " + j);
        if (CheckIfInsideGrid(new Vector2(i, j)))
        {
            if (j < 10)
                if (grid[i, j] != null && grid[i, j].tag == colorTag && !matchList.Contains(grid[i, j].gameObject))
                {
                    matchList.Add(grid[i, j].gameObject);
                    if (i < 16)
                        FloodFillAlgorithm(i + 1, j, colorTag); //re-do check to the right
                    if (i > 0)
                        FloodFillAlgorithm(i - 1, j, colorTag); //re-do check to the left
                    if (j < 10)
                        FloodFillAlgorithm(i, j + 1, colorTag); //re-do check up
                    if (j > 0)
                        FloodFillAlgorithm(i, j - 1, colorTag); //re-do check down
                }
        }
    }

    public List<GameObject> tempMatch = new List<GameObject>();
    public Color curColor;
    public void ClearAllMatches(int i, int j)
    {
        if (grid[i, j] == null)
            return;
        GameObject piece = grid[i, j].gameObject;
        FloodFillAlgorithm(i, j, piece.tag);


        if (matchList.Count >= 4)
        {
            if (i < width - 1 && j < height - 1)
                if (grid[i, j] != null && grid[i + 1, j] != null && grid[i, j + 1] != null && grid[i + 1, j + 1] != null)
                {
                    if (grid[i, j].tag == grid[i + 1, j].tag && grid[i, j].tag == grid[i, j + 1].tag && grid[i, j].tag == grid[i + 1, j + 1].tag)
                    {
                        Sprite sprite;
                        Material mat;
                        if (piece.tag == "Blue")
                        {
                            sprite = blueSprite;
                            mat = blueMat;
                            curColor = currentColors[0];
                        }
                        else
                        {
                            sprite = yellowSprite;
                            mat = yellowMat;
                            curColor = currentColors[1];
                        }
                        curColor.a = 256;
                        if (!tempMatch.Contains(grid[i, j].gameObject))
                        {
                   //         grid[i, j].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            grid[i, j].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i, j].gameObject);
                        }
                        if (!tempMatch.Contains(grid[i + 1, j].gameObject))
                        {
                            //    grid[i + 1, j].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i + 1, j].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i + 1, j].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i + 1, j].gameObject);
                        }
                        if (!tempMatch.Contains(grid[i, j + 1].gameObject))
                        {
                            //    grid[i, j + 1].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i, j + 1].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i, j + 1].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i, j + 1].gameObject);
                        }
                        if (!tempMatch.Contains(grid[i + 1, j + 1].gameObject))
                        {
                            //   grid[i + 1, j + 1].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                            //   grid[i + 1, j + 1].gameObject.GetComponent<SpriteRenderer>().color = curColor;
                            grid[i + 1, j + 1].gameObject.GetComponent<Renderer>().material = mat;
                            tempMatch.Add(grid[i + 1, j + 1].gameObject);
                        }
                    }
                }

            /*  for (int k = 0; k < matchList.Count; k++)
              {
                  matchList[k].gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                  Vector2 tempPos = Round(matchList[k].transform.position);
                  if (!tempMatch.Contains(matchList[k]))
                      tempMatch.Add(matchList[k]);
              }*/


        }

        matchList.Clear();

    }
    private IEnumerator DestroyLater()
    {

        while (true)
        {
            if (tempMatch.Count > 0)
            {
                GameObject go = tempMatch[0];
                if (go.transform.childCount == 1)
                {
                  //  Destroy(go.transform.GetChild(0).gameObject);
                    go.transform.GetChild(0).gameObject.AddComponent<TriangleExplosion>();
                    StartCoroutine(go.transform.GetChild(0).GetComponent <TriangleExplosion>().SplitMesh(true));
                    tempMatch.Remove(tempMatch[0]);
                    Instantiate(particleExplosion, go.transform.position, Quaternion.identity);
                    ScoreManager.instance.UpdateScore();
                }
                else
                {
                    tempMatch.Remove(tempMatch[0]);
                    Instantiate(particleExplosion, go.transform.position, Quaternion.identity);
                    GameObject.Destroy(go, 0.2f);
                    Vector2 tempPos = Round(go.transform.position);
                    grid[(int)tempPos.x, (int)tempPos.y] = null;
                    ScoreManager.instance.UpdateScore();
                    /*      if (trailHead.transform.position.x > tempMatch[0].transform.position.x && (trailHead.transform.position.x - tempMatch[0].transform.position.x <= 2f))
                          {


                          }*/
                 
                }
                if (tempMatch.Count == 0)
                {
                    GameObject.FindGameObjectWithTag("H").GetComponent<AudioSource>().Play();
                    GameManager.instance.currentPiece.GetComponent<PieceController>().isCleared = true;


                    Invoke("SettlePeicesinEmpty", 0.24f);
                }
            }

            yield return null;
        }
    }
    public IEnumerator TransitToPosition(GameObject target, Vector2 pos)
    {

        float waitForSeconds = 0.5f;
        float waitTime = 0;
        while (waitTime < waitForSeconds)
        {
            waitTime += Time.deltaTime / waitForSeconds;
            target.transform.position = Vector2.Lerp(target.transform.position, pos, waitTime);
          

            /* Original code
             *   target.transform.position = Vector2.Lerp(target.transform.position, pos, (waitTime / waitForSeconds));
            waitTime += Time.deltaTime;
            */
            yield return null;
        }
        StopCoroutine(TransitToPosition(null, Vector2.zero));

    }


    public float moveOffset;
    public List<int> pieceNumber = new List<int>();
    public List<GameObject> pieceExtra = new List<GameObject>();
    public void SettlePeicesinEmpty()
    {
        pieceNumber.Clear();
        pieceExtra.Clear();

        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, j] != null)
                {
                    GameObject square = grid[i, j].gameObject;
                    Vector2 pos = Round(square.transform.position);
                    int x = (int)(pos.x);
                    int y = (int)(pos.y);
                    if (CheckIfInsideGrid(square.transform.position))
                    {
                        for (int k = y - 1; k >= 0; k--)
                        {
                            if (grid[x, k] == null)
                            {

                                //Then Go Down, becuase you see? There is a fucking empty space :/
                                GameObject go = Instantiate(square, square.transform.position, square.transform.rotation);
                                Destroy(go, 0.55f);
                                pieceExtra.Add(go);
                                square.GetComponent<Renderer>().enabled = false;

                                #region Checking Conditions
                                if (x == 0 && grid[x + 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                                }
                                else if(x == width - 1 || x == 0)
                                {
                                    square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                                }
                                else if (x == 0 && grid[x + 1, k] == null || (grid[x + 1, k] == null && grid[x - 1, k] == null) || (x == width - 1 && grid[x - 1, k] == null))
                                {
                                    square.transform.position = new Vector2(x * moveOffset, (k * moveOffset));
                                }
                                else if (x == width - 1 && grid[x - 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x - 1, k].position.x - 1, grid[x - 1, k].position.y);
                                }
                                else if (grid[x + 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x + 1, k].position.x - 1, grid[x + 1, k].position.y);
                                }
                                else if (grid[x - 1, k] != null)
                                {
                                    square.transform.position = new Vector2(grid[x - 1, k].position.x + 1, grid[x - 1, k].position.y);
                                }
                                else
                                {
                                    Debug.Log("None of them is being Executed");
                                }
                                #endregion

                                pos = Round(square.transform.position);
                                x = (int)pos.x;                      //Use this X to transit
                                y = (int)pos.y;                      //Use this Y to transit



                                pieceNumber.Add(y);
                                UpdateIndividualGrid(square.transform);
                            }
                        }
                    }

                    //DO the transition work from here
                    if (pieceExtra != null && pieceExtra.Count > 0)
                    {
                        StartCoroutine(TransitToPosition(pieceExtra[0], new Vector2(square.transform.position.x, square.transform.position.y)));
                        pieceExtra.Clear();
                        StartCoroutine(EnableCheckingPhase(x, y));
                    }
                }
            }
        }
        //    if(tempMatch.Count == 0)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;
    }

    private IEnumerator EnableCheckingPhase(int x, int y)
    {
        yield return new WaitForSeconds(0.5f);
        if (grid[x, y] != null)
            grid[x, y].GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.1f);
        FinishClearing();
        //   StopAllCoroutines();

    }

    public void FinishClearing()
    {
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                ClearAllMatches(i, j);
            }
        }
        SettlePeicesinEmpty();
    }

    public bool CheckForStack(PieceController piece)
    {
        bool status = false;
        int count = 0;

        for (int i = 0; i < 4; i++)
        {
            Transform tempG = piece.transform.GetChild(i);
            Vector2 pos = Round(tempG.position);
            if ((int)pos.y < 1.2f)
            {
                count++;
                if (count == 4)
                {
                    status = true;
                    return status;
                }
            }
            else
            {
                if (grid[(int)pos.x, (int)pos.y - 1] != null)
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
        }
        Debug.Log("Status Checked With Code " + status + " And the Damn Count is " + count);

        return status;
    }
}
