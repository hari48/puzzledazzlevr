﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSize
{
    static Vector2 topRightCorner, edgeVector;
    static float height, width;
    public static float GetScreenToWorldHeight
    {
        get
        {
            topRightCorner = new Vector2(1, 1);
            edgeVector = Camera.main.ViewportToWorldPoint(topRightCorner);
            height = edgeVector.y * 2;
            return height;
        }
    }
    public static float GetScreenToWorldWidth
    {
        get
        {
            topRightCorner = new Vector2(1, 1);
            edgeVector = Camera.main.ViewportToWorldPoint(topRightCorner);
            width = edgeVector.x * 2;
            return width;
        }
    }

}
