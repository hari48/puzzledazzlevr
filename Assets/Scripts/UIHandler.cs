﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEditor;

public class UIHandler : MonoBehaviour
{
    //Static Variable
    public static UIHandler instance;

    public Sprite menu_ON, menu_OFF;
    //UI Components
    [Header("Menu")]

    public Sprite play_ON;
    public Sprite play_OFF;
    public Sprite options_ON, options_OFF;
    public Sprite help_ON, help_OFF;
    public Sprite quit_ON, quit_OFF;

    [Header("Options")]

    public GameObject music_ON;
    public GameObject music_OFF;
    public GameObject sound_ON;
    public GameObject sound_OFF;
    public GameObject vibration_ON;
    public GameObject vibration_OFF;
    public Sprite back_ON, back_OFF;

    [Header("Quit")]

    public Sprite Yes_ON;
    public Sprite  Yes_OFF;
    public Sprite No_ON, No_OFF;

    [Header("Paused")]

    public Sprite resume_ON;
    public Sprite resume_OFF;
    public Sprite restart_ON, restart_OFF;
 

    [Header("Result")]
    public Sprite retry_ON;
    public Sprite retry_OFF;
    public TextMeshProUGUI score_game;
    public TextMeshProUGUI highScore_game;
    public TextMeshProUGUI score_result;
    public TextMeshProUGUI highscore_result;
    public TextMeshProUGUI cleared_UI;

    [Header("GameObjects")]
    //Unity Objects
    public GameObject MainMenu;
    public GameObject HelpPanel;
    public GameObject ThemesPanel;
    public GameObject Options;
    public GameObject Quit;
    public GameObject Paused;
    public GameObject Result;
    public GameObject GamePlay;
 
    public int isSoundOn;
    public int isMusicOn;
    public int isVibrationOn;

    public GameObject popup;

    public Button SelectedButton;

    public TextMeshProUGUI[] coinsText;
    public TextMeshProUGUI targettoAchieve;
    public InputField nameI;
    private void Awake()
    {
        //Initializing instance of this script
        if(instance == null)
        {
            instance = this;
        }

  //      if(SelectedButton != null)
   //     SelectedButton.Select();
     
    }

    private void Start()
    {
        isSoundOn = PlayerPrefs.GetInt("sound", 1);
        isMusicOn = PlayerPrefs.GetInt("music", 1);
        isVibrationOn = PlayerPrefs.GetInt("vibration", 1);

        UpdateCoins(0);
    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape) && SceneManager.GetActiveScene().name == "Menu")
        {
            OnPressedQuit();
        }
    }
    private IEnumerator LoadSceneWithDelay(int level)
    {
        yield return new WaitForSeconds(0.55f);
        SceneManager.LoadScene(level);
    }

    private IEnumerator LoadSceneWithDelay(string level)
    {
        yield return new WaitForSeconds(0.55f);
        SceneManager.LoadScene(level);
    }

    #region Menu Buttons
    public void OnPressedPlay()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = play_ON;
        AudioHandler.Instance.FadeAudio();

        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);

        StartCoroutine(LoadSceneWithDelay(leveltoload));

    }



    public void OnPressedPlayWithModeName(string modeName)
    {

        if (modeName == "Survival")
        {
            PlayerPrefs.SetInt("isSurvival", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSurvival", 0);
        }
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = play_ON;
        AudioHandler.Instance.FadeAudio();
        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);
        StartCoroutine(LoadSceneWithDelay(leveltoload));

    }

    public void OnPressedPlayWithModeNameNoLoad(string modeName)
    {

        if (modeName == "Survival")
        {
            PlayerPrefs.SetInt("isSurvival", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isSurvival", 0);
        }
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = play_ON;
    //    AudioHandler.Instance.FadeAudio();
        int leveltoload = PlayerPrefs.GetInt("lastlevelPlayed", 3);
    //    StartCoroutine(LoadSceneWithDelay(leveltoload));

    }


    public void OnPressedPlayPotrait()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = play_ON;
        AudioHandler.Instance.FadeAudio();

        StartCoroutine(LoadSceneWithDelay("Level_Portrait"));

    }

    public void LoadLevelWithName(string name)
    {
        StartCoroutine(LoadSceneWithDelay(name));
    }

    public void OnPressedTheme()
    {

      //  currentButton.GetComponent<Image>().sprite = options_ON;     enable this later
        Invoke("EnableThemesPanel", 0.1f);
    }

    public void EnableThemesPanel()
    {
        UpdateCoins(0);
        ThemesPanel.SetActive(true);
        MainMenu.SetActive(false);
    }

    public void OnPressedOptions()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = options_ON;

        Invoke("EnableOptionsPanel", 0.1f);
    }

    public void EnableOptionsPanel()
    {
        MainMenu.SetActive(false);
        Options.SetActive(true);

        Button[] buttons = Options.GetComponentsInChildren<Button>();

        foreach(Button button in buttons)
        {
            if(button.name == "Back")
            {
                button.GetComponent<Image>().sprite = back_OFF;
           //     button.Select();
            }
        }

        if (isSoundOn == 1)
        {
            sound_ON.SetActive(true);
            sound_OFF.SetActive(false);
        }
        else
        {
            sound_ON.SetActive(false);
            sound_OFF.SetActive(true);
        }

        if (isMusicOn == 1)
        {
            music_ON.SetActive(true);
            music_OFF.SetActive(false);
        }
        else
        {
            music_ON.SetActive(false);
            music_OFF.SetActive(true);
        }
    }

    public void OnPressedHelp()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = help_ON;


        Invoke("EnableHelpPanel", 0.1f);
    }

    public void EnableHelpPanel()
    {
        MainMenu.SetActive(false);
        HelpPanel.SetActive(true);

        Button btn = HelpPanel.GetComponentInChildren<Button>();
    //    btn.Select();
    }

    public void OnPressedQuit()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = quit_ON;

        Button[] buttons = Quit.GetComponentsInChildren<Button>();


        buttons[0].gameObject.GetComponent<Image>().sprite = Yes_OFF;
        buttons[1].gameObject.GetComponent<Image>().sprite = No_OFF;

        Invoke("EnableQuitPanel", 0.1f);
    }

    public void EnableQuitPanel()
    {
        MainMenu.SetActive(false);
        Quit.SetActive(true);

        Button[] buttons = Quit.GetComponentsInChildren<Button>();


    //    buttons[0].Select();
    }
    #endregion

    #region Option Buttons
    
    public void OnPressedSound()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isSoundOn == 1)
        {
            sound_ON.SetActive(false);
            sound_OFF.SetActive(true);
            sound_OFF.GetComponent<Button>().Select();
            isSoundOn = 0;
            PlayerPrefs.SetInt("sound", 0);
        }
        else
        {
            sound_ON.SetActive(true);
            sound_ON.GetComponent<Button>().Select();
            sound_OFF.SetActive(false);
            isSoundOn = 1;
            PlayerPrefs.SetInt("sound", 1);
        }
    }

    public void OnPressedMusic()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isMusicOn == 1)
        {
            music_ON.SetActive(false);
            music_OFF.SetActive(true);
            music_OFF.GetComponent<Button>().Select();
            isMusicOn = 0;
            PlayerPrefs.SetInt("music", 0);
            AudioHandler.Instance.mainAudioSource.enabled = false;
        }
        else
        {
            music_ON.SetActive(true);
            music_ON.GetComponent<Button>().Select();
            music_OFF.SetActive(false);
            isMusicOn = 1;
            PlayerPrefs.SetInt("music", 1);
            AudioHandler.Instance.mainAudioSource.enabled = true;
        }
    }

    public void OnPressedVibration()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;


        if (isVibrationOn == 1)
        {
            vibration_ON.SetActive(false);
            vibration_OFF.SetActive(true);
            vibration_OFF.GetComponent<Button>().Select();
            isVibrationOn = 0;
            PlayerPrefs.SetInt("vibration", 0);
            
        }
        else
        {
            vibration_ON.SetActive(true);
            vibration_ON.GetComponent<Button>().Select();
            vibration_OFF.SetActive(false);
            isVibrationOn = 1;
            PlayerPrefs.SetInt("vibration", 1);
        }
    }

    public void OnPressedBackResult()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        if (currentButton.name == "No")
            currentButton.GetComponent<Image>().sprite = No_ON;
        else if (currentButton.name == "Back")
            currentButton.GetComponent<Image>().sprite = back_ON;
        else
            currentButton.GetComponent<Image>().sprite = menu_ON;

        AudioHandler.Instance.FadeAudio();
        Invoke("GoBackToMenu", 0.55f);

    }

    public void OnPressedBackTheme()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        Invoke("GoBackToMenu", 0.1f);
    }
    public void OnPressedBack()
    {
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;

        if (!currentButton.GetComponent<Button>())
            return;

        if(currentButton.name == "No")
            currentButton.GetComponent<Image>().sprite = No_ON;
       else if (currentButton.name == "Back")
            currentButton.GetComponent<Image>().sprite = back_ON;
        else
        currentButton.GetComponent<Image>().sprite = menu_ON;

        if (SceneManager.GetActiveScene().name == "Menu_portrait" || SceneManager.GetActiveScene().name == "Menu")
            Invoke("GoBackToMenu", 0.1f);
        else
        {
            if (SceneManager.GetActiveScene().buildIndex >= 3)
                SceneManager.LoadScene("Menu");
            else if (SceneManager.GetActiveScene().name == "Level_Portrait")
                SceneManager.LoadScene("Menu_portrait");
            isSoundOn = PlayerPrefs.GetInt("sound", 1);
            isMusicOn = PlayerPrefs.GetInt("music", 1);
        }

    }

    private void GoBackToMenu()
    {
        if (SceneManager.GetActiveScene().buildIndex >= 3)
            SceneManager.LoadScene("Menu");
        else if(SceneManager.GetActiveScene().name == "Level_Portrait")
            SceneManager.LoadScene("Menu_portrait");
        isSoundOn = PlayerPrefs.GetInt("sound", 1);
        isMusicOn = PlayerPrefs.GetInt("music", 1);

        MainMenu.SetActive(true);
        Options.SetActive(false);
        Quit.SetActive(false);
        Paused.SetActive(false);
       // Result.SetActive(false);
       if(HelpPanel != null)
        HelpPanel.SetActive(false);
        if (ThemesPanel != null)
            ThemesPanel.SetActive(false);

        Button[] buttons = MainMenu.GetComponentsInChildren<Button>();
     //   SelectedButton.Select();

        buttons[0].gameObject.GetComponent<Image>().sprite = play_OFF;
        buttons[3].gameObject.GetComponent<Image>().sprite = options_OFF;
        buttons[2].gameObject.GetComponent<Image>().sprite = help_OFF;
     //   buttons[3].gameObject.GetComponent<Image>().sprite = quit_OFF;
    }
    #endregion

    #region Quit Buttons
    public void OnPressedQuitYes()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;
        currentButton.GetComponent<Image>().sprite = Yes_ON;

        /*
        //int postScore = 0;
        int postScore = PlayerPrefs.GetInt("hs", 0);
        Debug.Log("PostScore is working");
        //Debug.Log(PlayerPrefs.GetInt("ENDLESS_HIGH_SCORE", 0) + "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        //Debug.Log(postScore + "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        JioGamesUnitySDKY.sInstance.PostScore(postScore);
        StartCoroutine(WaitTime());
        JioGamesUnitySDKY.sInstance.Recentupdate();
        StartCoroutine(WaitTime());
        Jio_AdsManager.Instance.Jio_POST_Roll.showAd();
        //Jio_AdsManager.Instance.Jio_POST_Roll.showAd();
        */


       

    //    Invoke("QuitApplication", 0.1f);
    }

    private void QuitApplication()
    {
        //Application.Quit();
    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(1);
    }

    public void OnPressedQuitNo()
    {
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = No_ON;
        Invoke("OnPressedBack", 0.1f);
    }

    #endregion

    #region Paused Buttons

    public void OnPressedPause()
    {
        Time.timeScale = 0;
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = false;
        AudioHandler.Instance.isPaused = true;
        AudioHandler.Instance.mainAudioSource.Pause();
        GamePlay.SetActive(false);
        Paused.SetActive(true);

        Button[] b = Paused.GetComponentsInChildren<Button>();
   
      //  b[0].Select();

        UpdateScoreUI();
    }
    public void OnPressedResume()
    {
        Time.timeScale = 1;
        AudioHandler.Instance.isPaused = false;
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = true;
        AudioHandler.Instance.mainAudioSource.UnPause();
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;
        currentButton.GetComponent<Image>().sprite = resume_ON;
        Invoke("EnableResume", 0.1f);
    }

    private void EnableResume()
    {

        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        GamePlay.SetActive(true);
        Paused.SetActive(false);
        if (!currentButton.GetComponent<Button>())
            return;



        Button[] buttons = Paused.GetComponentsInChildren<Button>();

        buttons[0].gameObject.GetComponent<Image>().sprite = resume_OFF;
        buttons[1].gameObject.GetComponent<Image>().sprite = restart_OFF;
        buttons[2].gameObject.GetComponent<Image>().sprite = menu_OFF;
    }
    public void OnPressedRestart()
    {
        Time.timeScale = 1;
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = restart_ON;
        Invoke("RestartGame", 0.1f);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnPressedRetry()
    {
        Time.timeScale = 1;
        GameObject currentButton = EventSystem.current.currentSelectedGameObject;
        if (!currentButton.GetComponent<Button>())
            return;

        currentButton.GetComponent<Image>().sprite = retry_ON;
        Invoke("RestartGame", 0.1f);
    }
    #endregion

    #region Result Buttons

    public void ShowResult()
    {
        GameManager.instance.isGameover = true;
        // Time.timeScale = 0;
        if (GameManager.instance.currentPiece.GetComponent<PieceController>() != null)
        GameManager.instance.currentPiece.GetComponent<PieceController>().enabled = false;
        FindObjectOfType<GameManager>().enabled = false;
        GamePlay.SetActive(false);
        Result.SetActive(true);
        Debug.Log("MidRoll is working");

#if UNITY_IOS
        if (ScoreManager.instance.score >= 60000  && PlayerPrefs.GetInt("reviewRequest", 0) == 0 && GameManager.instance.isGameover)
        {
            PlayerPrefs.SetInt("reviewRequest", 1);
            UnityEngine.iOS.Device.RequestStoreReview();
        }
#endif
#if UNITY_IOS
        StartCoroutine(IOSNotification.instance.RequestAuthorization());
        //      Jio_AdsManager.Instance.Jio_MID_Roll.showAd();
        //       Jio_AdsManager.Instance.Jio_MID_Roll.cacheAd();

        Leaderboards.instance.FetchScoresFromGameCentre();
#endif

        Button[] button = Result.GetComponentsInChildren<Button>();
   //     button[0].Select();
    }
#endregion

    public void UpdateScoreUI()
    {
        if (ScoreManager.instance.score < 10)
        {
            score_game.text = "0" + ScoreManager.instance.score;
            score_result.text = "0" + ScoreManager.instance.score;
            cleared_UI.text = "0" + ScoreManager.instance.cleared;
          
        }
        else
        {
            score_game.text = ScoreManager.instance.score.ToString();
            score_result.text = ScoreManager.instance.score.ToString();
            cleared_UI.text = ScoreManager.instance.cleared.ToString();

        }
        if(targettoAchieve != null)
        if(FindObjectOfType<LevelUniqueIdentifier>().levelTarget < 10)
        {
            targettoAchieve.text = "0" + FindObjectOfType<LevelUniqueIdentifier>().levelTarget;
        }
        else
        {
            targettoAchieve.text = "" + FindObjectOfType<LevelUniqueIdentifier>().levelTarget;
        }

        if (ScoreManager.instance.highScore < 10)
        {
            highScore_game.text = "0" + ScoreManager.instance.highScore;
            highscore_result.text = "0" + ScoreManager.instance.highScore;
        }
        else
        {
            highScore_game.text = ScoreManager.instance.highScore.ToString();
            highscore_result.text = ScoreManager.instance.highScore.ToString();
        }

       // coinsUI.text = GameManager.instance.Coins.ToString();
    }

    public void UpdateCoins(int amount)
    {
        int coins = PlayerPrefs.GetInt("coins", 0);
        coins += amount;

        foreach(TextMeshProUGUI t in coinsText)
        {
            t.text = coins.ToString();
        }

        PlayerPrefs.SetInt("coins", coins);
    }

    public void SetCharacterName()
    {
        if(nameI.text != null)
        {
            PlayerPrefs.SetString("PlayerName", nameI.text);
        }
    }
}
