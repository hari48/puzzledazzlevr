﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate512Cubes : MonoBehaviour
{
    public GameObject sampleCubePrefab;
    private GameObject[] sampleCube = new GameObject[512];
    public float maxScale;
    public Material mat;
    private void Start()
    {
        for(int i = 0; i < 56; i++)
        {
            GameObject instanceSampleCube = (GameObject)Instantiate(sampleCubePrefab);
            instanceSampleCube.transform.position = this.transform.position;
            instanceSampleCube.transform.parent = this.transform;
            instanceSampleCube.name = "SampleCube" + i;
            //       this.transform.eulerAngles = new Vector3(0, -0.703125f * i, 0);
            //        instanceSampleCube.transform.position = Vector3.right * 3 * i;
            instanceSampleCube.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 3 * i);
            sampleCube[i] = instanceSampleCube;
        }

        Color tempColor = mat.GetColor("_EmissionColor");
        x = tempColor.r; y = tempColor.g; z = tempColor.b;
    

    }

    private void ReturnColor()
    {
        if (r)
        {
            x = Mathf.Lerp(minimumx, maximumx, t);
            t += 0.2f * Time.deltaTime;
            if (t > 1.0f)
            {
                float temp = maximumx;
                maximumx = minimumx;
                minimumx = temp;
                t = 0.0f;
                r = !r;
                g = !g;
            }
        }
        if (g)
        {
            y = Mathf.Lerp(minimumx, maximumx, t);
            t += 0.2f * Time.deltaTime;
            if (t > 1.0f)
            {
                float temp = maximumy;
                maximumy = minimumy;
                minimumy = temp;
                t = 0.0f;
                g = !g;
                b = !b;
            }
        }
        if (b)
        {
            z = Mathf.Lerp(minimumx, maximumx, t);
            t += 0.2f * Time.deltaTime;
            if (t > 1.0f)
            {
                float temp = maximumz;
                maximumz = minimumz;
                minimumz = temp;
                t = 0.0f;
                b = !b;
                r = !r;
            }
        }
        Color newColor = new Color32((byte)x, (byte)y, (byte)z, (byte)0.5f);
        mat.SetColor("_EmissionColor", newColor);
        Debug.LogError("X = " + x + " Y = " + y + " z = " + z);

    }
    // animate the game object from -1 to +1 and back
    private float minimumx = 20f;
    private float maximumx = 191f;
    private float minimumy = 20f;
    private float maximumy = 191f;
    private float minimumz = 20f;
    private float maximumz = 191f;

    bool r = true, g = false, b = false;

    // starting value for the Lerp
    static float t = 0.0f;

    float x, y, z;
    public int band;
    private void Update()
    {
        /*  for(int i = 0; i < 512; i++)
          {
              if(sampleCube != null)
              {
                  sampleCube[i].transform.localScale = new Vector3(10, (AudioPeer.samples[i] * maxScale) + 2, 10);
              }
          }*/


        //      ReturnColor();

   //     Color color = new Color(AudioPeer.audioBandBuffer[band], AudioPeer.audioBandBuffer[band], AudioPeer.audioBandBuffer[band]);
      //  mat.SetColor("_EmissionColor", color);


        for (int i = 0; i < 56; i++)
        {
            if (sampleCube != null)
            {
       //         sampleCube[i].transform.localScale = new Vector3(10, (AudioPeer.samples[i] * maxScale) + 2, 10);
                sampleCube[i].transform.localScale = new Vector3(10, (AudioPeer.samples[i] * maxScale) + 2, 10);
            }
        }
    }
}
